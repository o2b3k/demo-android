package kg.shtraf.shtrafkg;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    public static void start(Context context) {
        context.startActivity(new Intent(context, LoginActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        final Button loginButton = findViewById(R.id.login_button);
        final EditText loginField = findViewById(R.id.login_form);

        loginButton.setOnClickListener(v -> {
            final String content = loginField.getText().toString();

            if (content.equals("12345")) {
                MainActivity.start(this);
            } else {
                Toast.makeText(this, "Неправилный ПИН", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
