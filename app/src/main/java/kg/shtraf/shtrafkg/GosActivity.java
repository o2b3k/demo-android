package kg.shtraf.shtrafkg;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class GosActivity extends AppCompatActivity {

    public static void start(Context context) {
        context.startActivity(new Intent(context, GosActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gos);

        final Button searchButton = findViewById(R.id.gos_search_button);
        final EditText number = findViewById(R.id.number_value);
        searchButton.setOnClickListener(v -> {
            final String content = number.getText().toString();
            if (content.equals("s8448j")){
                ResultPinActivity.start(this);
            }else {
                Toast.makeText(this, "Доступ запрещен", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
