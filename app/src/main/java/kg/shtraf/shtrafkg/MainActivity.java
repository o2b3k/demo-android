package kg.shtraf.shtrafkg;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static void start(Context context) {
        context.startActivity(new Intent(context, MainActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button buttonGos = findViewById(R.id.search_gos_b);
        final Button buttonPin = findViewById(R.id.search_pin_b);
        final Button buttonProtocol = findViewById(R.id.protocol_b);

        buttonGos.setOnClickListener(v -> GosActivity.start(this));
        buttonPin.setOnClickListener(v -> PinActivity.start(this));
        buttonProtocol.setOnClickListener(v -> {
            Toast.makeText(this, "Доступ запрещен", Toast.LENGTH_SHORT).show();
        });
    }
}
