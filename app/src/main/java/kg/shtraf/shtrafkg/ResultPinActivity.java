package kg.shtraf.shtrafkg;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class ResultPinActivity extends AppCompatActivity{

    public static void start(Context context) {
        context.startActivity(new Intent(context, ResultPinActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pin_result);
    }
}
