package kg.shtraf.shtrafkg;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

public class LauncherActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
    }

    @Override
    protected void onStart() {
        super.onStart();

        new Handler().postDelayed(this::openLoginActivity, 3000);
    }

    private void openLoginActivity() {
        LoginActivity.start(this);
        finish();
    }
}
