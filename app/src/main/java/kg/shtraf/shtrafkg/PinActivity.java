package kg.shtraf.shtrafkg;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

public class PinActivity extends AppCompatActivity{

    public static void start(Context context) {
        context.startActivity(new Intent(context, PinActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_pin);

        final Button searchButton = findViewById(R.id.pin_search_button);
        searchButton.setOnClickListener(v -> ResultPinActivity.start(this));
    }
}
